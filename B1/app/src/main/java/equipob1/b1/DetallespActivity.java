package equipob1.b1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DetallespActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detallesp);
        final String[] contactos = new String[]{"Enrique","Yael","Jorge","Castell","Elena"};
        ListView lista = (ListView) findViewById(R.id.dp_lista_comtactos);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,contactos);
        lista.setAdapter(adapter);

        TextView nombre = (TextView) findViewById(R.id.dp_nombre);
        TextView descripcion = (TextView) findViewById(R.id.dp_descripcion);
        TextView monto = (TextView) findViewById(R.id.dp_monto);
        TextView fecha = (TextView) findViewById(R.id.dp_fecha);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null)
        {
            nombre.setText("Nombre: "+bundle.getString("nombreP"));
            descripcion.setText("Descripción: "+bundle.getString("descripcion"));
            monto.setText("Monto: "+bundle.getString("monto"));
            fecha.setText("Fecha límite: "+bundle.getString("fecha"));
        }



        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(DetallespActivity.this, "Invitaste a " + contactos[position], Toast.LENGTH_SHORT).show();
            }
        });


    }
}
