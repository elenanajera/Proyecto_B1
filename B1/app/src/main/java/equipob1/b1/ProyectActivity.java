package equipob1.b1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Vector;

public class ProyectActivity extends AppCompatActivity {

    EditText nombre,descripcion,monto,fecha;

    ListView lista;
    String nombreP,descripcionP,fechaP;
    float montoP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyect);
        Button crear_p = (Button) findViewById(R.id.p_crear_proyecto);
        lista = (ListView) findViewById(R.id.p_lista_proyectos);
        final ArrayList<String> nombres = new ArrayList<String>();
        final ArrayList<String> descripciones = new ArrayList<String>();
        final ArrayList<String> fechas = new ArrayList<String>();
        final ArrayList<String> montos = new ArrayList<String>();


        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,nombres);
        lista.setAdapter(adapter);
        nombre = (EditText) findViewById(R.id.p_nombre);
        descripcion = (EditText) findViewById(R.id.p_descripcion);
        monto = (EditText) findViewById(R.id.p_monto);
        fecha = (EditText) findViewById(R.id.p_fecha_limite);
        crear_p.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombres.add(nombre.getText().toString());
                descripciones.add(descripcion.getText().toString());
                fechas.add(fecha.getText().toString());
                montos.add(monto.getText().toString());
                adapter.notifyDataSetChanged();
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ProyectActivity.this,DetallespActivity.class);
                intent.putExtra("nombre",lista.getItemIdAtPosition(position));
                intent.putExtra("nombreP",nombres.get(position).toString());
                intent.putExtra("descripcion",descripciones.get(position).toString());
                intent.putExtra("monto",montos.get(position).toString());
                intent.putExtra("fecha",fechas.get(position).toString());
                startActivity(intent);
            }
        });

    }

    public void createProject(View view){


    }

}
